# Integrations for APITUBE

#### See integrations in other languages(and frameworks) below:

- [C](https://gitlab.com/apitube/integrations/tree/master/C)
- [C#](https://gitlab.com/apitube/integrations/tree/master/C#)
- [Clojure](https://gitlab.com/apitube/integrations/tree/master/Clojure)
- [Go](https://gitlab.com/apitube/integrations/tree/master/Go)
- [HTTP](https://gitlab.com/apitube/integrations/tree/master/HTTP)
- [Java](https://gitlab.com/apitube/integrations/tree/master/Java)
- [Javascript](https://gitlab.com/apitube/integrations/tree/master/Javascript)
- [Kotlin](https://gitlab.com/apitube/integrations/tree/master/Kotlin)
- [Node.js](https://gitlab.com/apitube/integrations/tree/master/Node.js)
- [OCaml](https://gitlab.com/apitube/integrations/tree/master/OCaml)
- [Objective-C](https://gitlab.com/apitube/integrations/tree/master/Objective-C)
- [PHP](https://gitlab.com/apitube/integrations/tree/master/PHP)
- [Powershell](https://gitlab.com/apitube/integrations/tree/master/Powershell)
- [Python](https://gitlab.com/apitube/integrations/tree/master/Python)
- [R](https://gitlab.com/apitube/integrations/tree/master/R)
- [RapidQL](https://gitlab.com/apitube/integrations/tree/master/RapidQL)
- [Ruby](https://gitlab.com/apitube/integrations/tree/master/Ruby)
- [Shell](https://gitlab.com/apitube/integrations/tree/master/Shell)
- [Swift](https://gitlab.com/apitube/integrations/tree/master/Swift)
